FROM openjdk:8-jre-alpine

EXPOSE 8080

COPY ./target/Spring-boot-Payment-Gateway-with-PayPal-*.jar /usr/app

WORKDIR /usr/app

CMD java -jar Spring-boot-Payment-Gateway-with-PayPal-*.jar



