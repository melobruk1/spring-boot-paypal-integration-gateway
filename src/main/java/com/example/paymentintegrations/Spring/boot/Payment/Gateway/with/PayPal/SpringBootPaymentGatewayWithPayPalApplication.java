package com.example.paymentintegrations.Spring.boot.Payment.Gateway.with.PayPal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootPaymentGatewayWithPayPalApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootPaymentGatewayWithPayPalApplication.class, args);
	}

}
